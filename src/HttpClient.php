<?php

namespace Develia;

require_once ("HttpRequest.php");

class HttpClient
{
    private $base_url;

    public function __construct($base_url)
    {

        $this->base_url = $base_url;
    }

    /**
     * Crea una nueva instancia de HttpRequest a partir de una URL base y un path.
     *
     * @param string $path   El path de la solicitud.
     * @param string $method El método HTTP a utilizar para la solicitud (GET, POST, PUT, DELETE, etc.).
     *
     * @return HttpRequest Retorna una instancia de HttpRequest con la URL y el método proporcionados.
     */
    public function create_request($path, $method)
    {
        // Eliminar cualquier barra diagonal adicional al final de la URL
        $url = rtrim($this->base_url, '/');

        // Eliminar cualquier barra diagonal adicional al principio del path
        $path = ltrim($path, '/');

        // Concatenar la URL y el path con una barra diagonal en el medio
        $url_path = $url . '/' . $path;

        return new HttpRequest($url_path, $method);
    }
}

