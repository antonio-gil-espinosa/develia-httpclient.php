<?php

namespace Develia;

class HttpResponse
{
    private $response_code;
    private $response_content;
    private $response_type;

    public function __construct($response_code, $response_content,$response_type)
    {

        $this->response_code = $response_code;
        $this->response_content = $response_content;
        $this->response_type = $response_type;
    }

    /**
     * Get the HTTP response code
     *
     * @return int
     */
    public function getResponseCode()
    {
        return $this->response_code;
    }

    /**
     * Get the formatted content based on the response type
     *
     * @return mixed
     */
    public function getContent()
    {
        if ($this->response_type == "application/json")
            return $this->getJson();

        return $this->response_content;
    }

    /**
     * Get the raw content of the HTTP response
     *
     * @return string
     */
    public function getRawContent()
    {
        return $this->response_content;
    }

    /**
     * Get the JSON decoded content if the response type is JSON
     *
     * @return mixed
     */
    public function getJson()
    {
        return json_decode($this->getContent());
    }

    /**
     * Get the HTTP response content type
     *
     * @return string
     */
    public function getResponseType()
    {
        return $this->response_type;
    }
}