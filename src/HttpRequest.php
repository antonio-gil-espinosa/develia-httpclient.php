<?php

namespace Develia;

require_once("HttpResponse.php");

class HttpRequest
{
    private $url;
    private $method;
    private $data = [];
    private $query = [];
    private $headers = [];

    public function __construct($url, $method = "get")
    {

        $this->url = $url;
        $this->method = strtoupper($method);
    }

    public function setPostData($data)
    {
        $this->data = $data;
        return $this;
    }

    public function setQuery($query)
    {
        $this->query = $query;
        return $this;
    }

    public function setHeaders($headers)
    {
        $this->headers = $headers;
        return $this;
    }

    public function setHeader($key, $value)
    {
        $this->headers[$key] = $value;
        return $this;
    }

    public function setContentType($value)
    {
        $this->setHeader("Content-Type", $value);
    }

    public function getContentType()
    {
        if (array_key_exists("Content-Type", $this->headers))
            return $this->headers["Content-Type"];
        return null;
    }

    public function getPostData()
    {
        return $this->data;
    }

    public function getQuery()
    {
        return $this->query;
    }

    public function getHeaders()
    {
        return $this->headers;
    }

    private $host_verification = true;
    private $peer_verification = true;
    /**
     * @return HttpResponse
     * @throws \Exception
     */
    public function execute()
    {

        $curl = curl_init();
        $url = $this->url;
        if ($this->query) {
            $url .= "?" . http_build_query($this->query);
        }

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, $this->host_verification ? 1 : 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, $this->peer_verification ? 1 : 0);

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $this->method);

        if ($this->data) {

            if ($this->getContentType() === 'application/json') {
                $postData = json_encode($this->data);
            } else {
                $postData = http_build_query($this->data);
            }

            curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
        }


        if ($this->headers) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, $this->headers);
        }


        $response = curl_exec($curl);


        if (curl_errno($curl)) {
            $exception = new \Exception(curl_error($curl));
            curl_close($curl);
            throw $exception;
        }

        $response = new HttpResponse(curl_getinfo($curl, CURLINFO_HTTP_CODE), $response, curl_getinfo($curl, CURLINFO_CONTENT_TYPE));
        curl_close($curl);

        return $response;
    }

    /**
     * @param mixed $host_verification
     * @return HttpRequest
     */
    public function setHostVerification($host_verification)
    {
        $this->host_verification = $host_verification;
        return $this;
    }

    /**
     * @param mixed $peer_verification
     * @return HttpRequest
     */
    public function setPeerVerification($peer_verification)
    {
        $this->peer_verification = $peer_verification;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHostVerification()
    {
        return $this->host_verification;
    }

    /**
     * @return mixed
     */
    public function getPeerVerification()
    {
        return $this->peer_verification;
    }

}